import 'Styles/main.scss';

import Vue from 'vue';
import Vuelidate from 'vuelidate';
import router from 'Router';
import store from 'Store';

import App from 'Pages/App.vue';

// ----------------------------------------
// main runtime

Vue.use(Vuelidate);

export default new Vue({
  router,
  store,
  el: '#app',
  render: h => h(App),
});
