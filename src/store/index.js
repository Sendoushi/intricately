import Vue from 'vue';
import Vuex from 'vuex';
import theme from './theme';
import breadcrumb from './breadcrumb';

Vue.use(Vuex);

// ----------------------------------------
// export

export default new Vuex.Store({
  modules: {
    theme,
    breadcrumb,
  },
});
