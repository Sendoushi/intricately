// ----------------------------------------
// variables

// ----------------------------------------
// methods

// ----------------------------------------
// export

export default {
  namespaced: true,
  state: {
    backgroundColor: '#fff',
  },
  getters: {},
  mutations: {
    setBackground(state, newColor) {
      state.backgroundColor = newColor;
    },
  },
  actions: {
    setBackground(context, newColor) {
      context.commit('setBackground', newColor);
    },
    unsetBackground(context) {
      context.commit('setBackground', '#fff');
    },
  },
};
