// ----------------------------------------
// variables

// ----------------------------------------
// methods

// ----------------------------------------
// export

export default {
  namespaced: true,
  state: {
    list: [],
  },
  getters: {},
  mutations: {
    set(state, list) {
      state.list = list;
    },
    empty(state) {
      state.list = [];
    },
  },
  actions: {
    set(context, list) {
      context.commit('set', list);
    },
  },
};
