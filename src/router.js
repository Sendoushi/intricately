import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from 'Pages/Home.vue';
import CompanyData from 'Pages/CompanyData.vue';
import CompanyPage from 'Pages/CompanyPage.vue';
import CompanyTable from 'Pages/CompanyTable.vue';

// ----------------------------------------
// variables

const routes = [
  { path: '/', name: 'home', component: Home },
  { path: '/company-data', name: 'company-data', component: CompanyData },
  { path: '/company-page', name: 'company-page', component: CompanyPage },
  { path: '/company-table', name: 'company-table', component: CompanyTable },
];

const router = new VueRouter({
  routes,
});

// ----------------------------------------
// singleton runtime

Vue.use(VueRouter);

export default router;
