const { assert } = require('chai');
const { mount } = require('@vue/test-utils');
const ModalNotes = require('../../src/components/ModalNotes.vue');

// --------------------------------------------------
// variables

// TODO: eventually we want to use import in mocha aswell
const Comp = ModalNotes.default;

// --------------------------------------------------
// test suite

describe('components: ModalNotes', () => {
  it('renders and sets the notes from the original ones', () => {
    const notes = 'Hello';
    const res = mount(Comp, { propsData: { originalNotes: notes } });

    assert.equal(res.vm.$data.notes, notes);
    assert.ok(res.contains('textarea'));
  });
});
