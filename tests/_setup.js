// needed to make sure we have a window env
require('jsdom-global')();

// DEV: https://github.com/vuejs/vue-test-utils/issues/936
window.Date = Date;
