const { assert } = require('chai');
const breadcrumb = require('../../src/store/breadcrumb');

// --------------------------------------------------
// variables

// TODO: eventually we want to use import in mocha aswell
const store = breadcrumb.default;

// --------------------------------------------------
// test suite

describe('store: breadcrumb', () => {
  describe('mutation: set', () => {
    it('runs and sets', () => {
      // prepare the test
      const list = [{ name: 'foo', path: 'bar' }];
      store.state = [];

      // run the test
      store.mutations.set(store.state, list);

      assert.ok(store.state);
      assert.ok(store.state.list);
      assert.equal(store.state.list.length, list.length);

      for (let i = 0; i < list.length; i += 1) {
        const originalSingle = list[i];
        const found = store.state.list.filter((single) => {
          const isName = originalSingle.name === single.name;
          const isPath = originalSingle.path === single.path;

          return isName && isPath;
        });
        assert.equal(found.length, 1);
      }
    });
  });

  describe('mutation: empty', () => {
    it('runs and empties', () => {
      // prepare the test
      store.state = [{ name: 'foo', path: 'bar' }];

      // run the test
      store.mutations.empty(store.state);

      assert.ok(store.state);
      assert.ok(store.state.list);
      assert.equal(store.state.list.length, 0);
    });
  });
});
